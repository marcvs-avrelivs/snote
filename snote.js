#!/usr/bin/env node
const path = require('path');
const axios = require('axios');
const cheerio = require('cheerio');
const getopt = require('node-getopt');
const rl = require('readline-sync');
const {ObjectId, MongoClient} = require('mongodb');
const MongoUrl = 'mongodb://localhost:27017';

/**
* snote - take a note!
*/
class Snote {
  #mongo = null;
  #db = null;
  #entries = null;

  /**
  * make a deng object
  */
  constructor() {
    const opts = [
      ['c', 'category=CAT', 'category to insert note for'],
      ['l', 'list', 'list notes for category'],
      ['g', 'get=ID', 'get note with id :ID'],
      ['d', 'delete=ID', 'delete note with id :ID'],
      ['D', 'delete-cat', 'delete category'],
      ['h', 'help', 'display this help'],
      ['x', 'omit-id', 'omit id\'s from listings'],
      ['X', 'include-date', 'include added date in listing'],
      ['v', 'version', 'show version'],
    ];
    const help = 'Usage: ' + path.basename(process.argv[1]) +
    ' [OPTIONS] [NOTE?]\n\n[[OPTIONS]]\n\n';
    this.opt = getopt.create(opts);
    this.opt.setHelp(help);
    this.opt.bindHelp().parseSystem();
    return (async () => {
      this.#mongo = await MongoClient.connect(MongoUrl, {
        useUnifiedTopology: true,
      });
      this.#db = this.#mongo.db('snote');
      this.#entries = this.#db.collection('entries');
      return this;
    })();
  }
  /**
   * get note from database
   * @param {string} noteId - mongo objectid for note
   */
  async getNote(noteId) {
    const oid = new ObjectId(noteId);
    const note = await this.#entries.findOne({'_id': oid});
    if (note) {
    } else {
    }
    return note;
  }
  /**
  * delete note from db
  * @param {string} id - mongo id of note
  */
  async deleteNote(id) {
    const oid = new ObjectId(id);
    const q = await this.#entries.findOne({'_id': oid});
    if (!q) {
      console.log('Note with id', id, 'not found!');
      return false;
    } else {
      return await this.#entries.deleteOne({'_id': oid});
    }
  }

  /**
  * delete category from db
  * @param {string} name - name of category
  */
  async deleteCategory(name) {
    const cats = await this.#entries.distinct('category', {});
    if (!cats.includes(name)) {
      console.log('Category not found:', name);
      return false;
    }
    const conf = rl.question(
        'Are you sure? This will destroy all of it! (y/N): ',
    );
    if (conf.charAt(0).toLowerCase()==='y') {
      console.log('Here are all the notes for that category:');
      await this.listNotes(name, {noID: true});
      console.log('Deleting category', name);
      return await this.#entries.deleteMany({category: name});
    } else {
      console.log('OK 👌');
      return false;
    }
  }
  /**
  * add note to db
  * @param {string} category - category
  * @param {string} note - note to add
  */
  async addNote(category, note) {
    const doc = {
      category,
      note,
      timestamp: Date.now(),
    };
    const res = await this.#entries.insertOne(doc);
    let ret;
    if (res) {
      ret = res.insertedId;
    } else {
      ret = false;
    }
    return ret;
  }
  /**
   * get notes for category
   * @param {string} cat - category
   */
  async getNotes(cat) {
    const res = await this.#entries.find({category: cat});
    let ret;
    if (res) {
      ret = res.toArray();
    } else {
      ret = false;
    }
    return ret;
  }
  /**
   * output category list
   */
  async listCategories() {
    const cats = await this.#entries.distinct('category', {});
    console.log('Categories');
    cats.forEach((e) => {
      console.log(`${e}`);
    });
  }
  /**
   * output note list for category
   * @param {string} cat - category
   * @param {Object} opts - options
   *    (noID: omit id from listing, date: add date to listing)
   */
  async listNotes(cat, opts) {
    const notes = await this.getNotes(cat);
    const {noID, date} = opts;
    console.log('Notes for '+cat);
    notes.forEach((e) => {
      const out = [];
      if (!noID) {
        out.push(e['_id'] +': ');
      }
      out.push(e.note);
      if (date) {
        const ts = new Date(e.timestamp).toLocaleString();
        out.push(` (added ${ts})`);
      }
      console.log(out.join(''));
    });
  }
  /**
   * actually run the app lols
   */
  async run() {
    // ['c', 'category=CAT', 'category to insert note for'],
    // ['l', 'list', 'list notes for category'],
    // ['g', 'get=ID', 'get note with id :ID'],
    // ['h' 'help', 'display this help'],
    // ['v' 'version', 'show version']
    const opts = this.opt.options;
    const note = this.opt.argv.join(' ');
    const cat = opts.category;
    if (opts.get) {
      const {category, note} = await this.getNote(opts.get);
      console.log('Category:', category);
      console.log(note);
    } else if (opts.list) {
      if (!cat) {
        await this.listCategories();
      } else {
        await this.listNotes(cat, {noID: opts['omit-id'], date: opts['include-date']});
      }
    } else if (opts.version) {
      console.log('vo.π.∞');
    } else if (opts.delete) {
      const res = await this.deleteNote(opts.delete);
      if (res) {
        console.log('Note deleted successfully');
      } else {
        console.log('Unable to delete note');
      }
    } else if (opts['delete-cat']) {
      if (!cat) {
        console.log('Please provide a category');
      } else {
        const res = await this.deleteCategory(cat);
        if (res) {
          console.log('Category deleted owo');
        } else if (res === false) {
        } else {
          console.log('Could not delete');
        }
      }
    } else {
      if (!cat) {
        console.log('pls provide category to insert note under');
      } else if (!note || note == '') {
        console.log('pls provide a note to add');
      } else {
        const id = await this.addNote(cat, note);
        if (id) {
          console.log('Note added with ID:', id);
        } else {
          console.log('Note could not be added owo');
        }
      }
    }
    this.#mongo.close();
  }
  /**
  * kill it with fire
  */
  destructor() {
    this.#mongo.close();
  }
}

(async () => {
  const snote = await new Snote();
  snote.run();
})();
